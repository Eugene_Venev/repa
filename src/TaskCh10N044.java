import java.util.Scanner;

public class TaskCh10N044 {
    static class Factorial {
        int digiRoot(int n) {
            int result = 0;
            int sum = n;
            while (sum > 0) {
                result += sum % 10;
                sum = sum / 10;
            }
            if (result >= 10)
                result = digiRoot(result);
            return result;
        }
    }

    static class Recursion {
        public static void main(String args[]) {
            Factorial f = new Factorial();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите число: ");
            int num = str.nextInt();
            System.out.println(f.digiRoot(num));
        }
    }
}