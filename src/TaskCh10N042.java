import java.util.Scanner;

public class TaskCh10N042 {

    static class Factorial {
        int pow(int m, int n) {
            int result;
            if (n == 1) return m;
            result = m * pow(m, n - 1);
            return result;

        }
    }

    static class Recursion {
        public static void main(String args[]) {
            Factorial f = new Factorial();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите число: ");
            int num = str.nextInt();
            System.out.println("Введите степень числа: ");
            int powNum = str.nextInt();
            System.out.println(f.pow(num, powNum));
        }
    }
}
