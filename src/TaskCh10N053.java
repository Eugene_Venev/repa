import java.util.Scanner;

public class TaskCh10N053 {
    static class recurs {

        int inverseArr(int[] arr, int arrlen) {
            if (arrlen == 0) {
                return 0;
            } else {
                System.out.println(arr[arrlen - 1]);
                return inverseArr(arr,arrlen-1);
            }
        }
    }

    static class Recursion {
        public static void main(String args[]) {
            recurs f = new recurs();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите систему счисления: ");
            int arrSize = str.nextInt();
            int[] arr = new int[arrSize];
            int arrlen = arr.length;
            System.out.println("Введите элемены массива: ");
            int i;
            for (i = 0; i < arr.length; i++) {
                arr[i] = str.nextInt();
            }
            System.out.println(f.inverseArr(arr, arrlen));
        }
    }
}
