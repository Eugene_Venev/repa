import java.util.Scanner;

class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);

        System.out.println("сейчас месяц");
        int today_m = str.nextInt();

        System.out.println("сейчас год");
        int today_y = str.nextInt();

        System.out.println("месяц рождения");
        int bd_m = str.nextInt();

        System.out.println("год рождения");
        int bd_y = str.nextInt();

        int full_age = 0;

        if (today_m >= bd_m) {
            full_age = today_y - bd_y;
        } else if (today_m < bd_m) {
            full_age = today_y - bd_y - 1;
        }

        System.out.println("полных лет " + full_age);

    }
}
