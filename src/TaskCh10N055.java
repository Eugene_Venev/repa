import java.util.Scanner;

class TaskCh10N055 {
    static class recurs {
        public static int meth(){
            Scanner str = new Scanner(System.in);
            System.out.println("Введите систему счисления: ");
            return str.nextInt();
        }

        public static String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};

        public static String inverse(int num, int meth) {
            recurs m = new recurs();
            if (num == 0)
                return "";
             else {
                return inverse(num / meth, meth) + hex[num % meth];
            }
        }
    }

    static class translate {
        public static void main(String[] args) {
            recurs r = new recurs();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите число: ");
            int num = str.nextInt();
            System.out.println(r.inverse(num, r.meth()));
        }
    }
}
