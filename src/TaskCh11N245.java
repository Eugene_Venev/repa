import java.util.Arrays;
import java.util.Scanner;

public class TaskCh11N245 {
    static class Outer {
        public static void main(String[] args) {
            Scanner str = new Scanner(System.in);
            System.out.println("Введите размер массива: ");
            int arrSize = str.nextInt();
            int[] arr = new int[arrSize];
            int[] minArr = new int[arrSize];
            int[] pluArr = new int[arrSize];
            System.out.println("Введите элемены массива: ");
            int i, l;
            int j = 0;
            int k = 0;
            int count = 0;
            for (l = 0; l < arr.length; l++) {
                arr[l] = str.nextInt();
            }
            for (i = 0; i < arr.length; i++) {
                if (arr[i] < 0) {
                    System.arraycopy(arr, i, minArr, j, 1);
                    count++;
                    j++;
                } else if (arr[i] >= 0) {
                    System.arraycopy(arr, i, pluArr, k, 1);
                    k++;
                }
            }
            System.arraycopy(pluArr, 0, minArr, count, pluArr.length - count);
            for (i = 0; i < arr.length; i++) System.out.println(minArr[i]);
        }
    }
}
