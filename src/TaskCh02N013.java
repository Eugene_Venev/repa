import java.util.Scanner;

class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        int num = str.nextInt();
        int mun = (num % 10) * 100 + ((num % 100) / 10) * 10 + num / 100;
        System.out.println("Получается число " + mun);
    }
}
