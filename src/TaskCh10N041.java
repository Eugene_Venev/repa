import java.util.Scanner;

public class TaskCh10N041 {



    static class Factorial {
        int fact(int n) {
            int result;
            if ((n == 1) || (n == 0)) return 1;
            result = fact(n - 1) * n;
            return result;

        }
    }

    static class Recursion {
        public static void main(String args[]) {
            Factorial f = new Factorial();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите число: ");
            int num = str.nextInt();
            System.out.println(f.fact(num));
        }
    }
}
