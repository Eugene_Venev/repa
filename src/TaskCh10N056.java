import java.util.Scanner;

public class TaskCh10N056 {
    static class recurs {
        boolean simNum(int number) {
            if (number == 0 || number == 1)
                return true;
            return div(number, 2);
        }

        boolean div(int num, int natDiv) {
            if (num == natDiv)
                return true;
            if (num % natDiv == 0)
                return false;
            else
                return div(num, natDiv + 1);
        }
    }

    static class Recursion {
        public static void main(String args[]) {
            recurs f = new recurs();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите число для проверки: ");
            int num = str.nextInt();
            System.out.println(f.simNum(num));
        }
    }
}