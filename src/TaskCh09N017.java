import java.util.Scanner;

class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.println("Строка: ");
        String obj1 = str.nextLine();
        if (obj1.charAt(0) == obj1.charAt(obj1.length()-1))
            System.out.println("Верно");
        else
            System.out.println("Неверно");
    }
}