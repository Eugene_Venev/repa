import java.util.Scanner;

public class TaskCh10N043 {

    public static int summNum(int num) {

        if (num < 10) {
            return num;
        } else {
            return num % 10 + summNum(num / 10);
        }
    }

    public static int summBits(int num) {

        if (num < 10) {
            return 1;
        } else {
            return 1 + summBits(num / 10);
        }
    }

    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.println("Enter the number");
        int num = str.nextInt();
        System.out.println(summNum(num));
        System.out.println(summBits(num));
    }
}

