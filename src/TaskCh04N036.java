import java.util.Scanner;

class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);

        System.out.println("число:");
        int num = str.nextInt();
        int colo = num % 5;
        if (colo < 3) {
            System.out.println("зеленый ");
        } else if (colo > 3) {
            System.out.println("красный ");
        } else if (colo == 3) {
            System.out.println("переходный процесс изменения цвета ");
        }
    }
}