import java.util.Scanner;

public class TaskCh10N048 {
    static class recurs {

        int maxArr(int[] arr, int arrlen) {
            if (arrlen > 1) {
                return Math.max(arr[arrlen - 1], maxArr(arr, arrlen - 1));
            }
            return arr[0];
        }
    }
    static class Recursion {
        public static void main(String args[]) {
            recurs f = new recurs();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите размер массива: ");
            int arrSize = str.nextInt();
            int[] arr = new int[arrSize];
            int arrlen = arr.length;
            System.out.println("Введите элемены массива: ");
            int i;
            for (i = 0; i < arr.length; i++) {
                arr[i] = str.nextInt();
            }
            System.out.println(f.maxArr(arr, arrlen));
        }
    }
}