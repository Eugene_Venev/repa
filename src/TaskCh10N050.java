public class TaskCh10N050 {
    static class recurs {
        int akerman(int n, int m) {
            if (n == 0)
                return m + 1;
            if (n != 0 && m == 0)
                return akerman(n - 1, 1);
            if (n > 0 && m > 0)
                return akerman(n - 1, akerman(n, m - 1));
            return -1;
        }
    }

    public static void main(String[] args) {
        recurs f = new recurs();
        System.out.println(f.akerman(4, 0));
    }
}

