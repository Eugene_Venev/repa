import java.util.Scanner;

public class TaskCh09N022 {
    static void halfWord(String row, int Ind) {
        System.out.print(row.charAt(Ind));

        if ((Ind + 1) < (row.length() / 2)) {
            halfWord(row, Ind + 1);
        }
    }

    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.println("слово с четным количеством букв: ");
        String row = str.nextLine();
        halfWord(row, 0);


    }
}

