import java.util.Scanner;

class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);

        System.out.println("число:");
        int num = str.nextInt();

        if (num == 0) {
            System.out.println("ноль ");
        } else if (num % 2 == 0) {
            System.out.println("четное ");
        } else if (num % 2 != 0) {
            System.out.println("нечетное ");
        }
    }
}
