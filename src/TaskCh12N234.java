import java.util.Arrays;
import java.util.Scanner;

public class TaskCh12N234 {
    static class intro {
        public static void main(String[] Args) {
            Scanner str = new Scanner(System.in);
            System.out.println("Введите размер массива: ");
            int arrSize = str.nextInt();
            int[][] arr = new int[arrSize][arrSize];
            int[][] arrCol = Arrays.copyOf(arr, arrSize);
            int i, j;
            for (i = 0; i < arr.length; i++) {
                System.out.println(" ");
                for (j = 0; j < arr.length; j++) {
                    arr[i][j] = ((int) (Math.random() * 31) + 1);
                    System.out.print(arr[i][j] + "\t");
                }
            }
            System.out.println("  ");
            System.out.println("Номер строки: ");
            int line = str.nextInt();
            System.out.println("Номер столбца: ");
            int column = str.nextInt();
            output o = new output();
            o.outPut(arr, line);
            System.out.println("  ");
            o.outPutCol(arrCol, column);
        }
    }

    static class processing {
        public int[][] cuttingLine(int[][] arr, int line) {
            int i, j;
            for (i = line - 1; i < arr.length; i++) {
                for (j = 0; j < arr.length; j++) {
                    if (i == arr.length - 1) arr[i][j] = 0;
                    else arr[i][j] = arr[i + 1][j];
                }
            }
            return arr;
        }

        public int[][] cuttingColumn(int[][] arrCol, int column) {
            int i, j;
            for (i = 0; i < arrCol.length; i++) {
                for (j = column - 1; j < arrCol.length; j++) {
                    if (j == arrCol.length - 1) arrCol[i][j] = 0;
                    else arrCol[i][j] = arrCol[i][j + 1];
                }
            }
            return arrCol;
        }
    }

    static class output {
        void outPut(int[][] arr, int line) {
            processing p = new processing();
            int[][] exit = p.cuttingLine(arr, line);
            int i, j;
            for (i = 0; i < arr.length; i++) {
                System.out.println(" ");
                for (j = 0; j < arr.length; j++) {
                    System.out.print(exit[i][j] + "\t");
                }
            }
        }

        void outPutCol(int[][] arrCol, int column) {
            processing p = new processing();
            int[][] exitColumn = p.cuttingColumn(arrCol, column);
            int i, j;
            for (i = 0; i < arrCol.length; i++) {
                System.out.println(" ");
                for (j = 0; j < arrCol.length; j++) {
                    System.out.print(exitColumn[i][j] + "\t");
                }
            }
        }
    }
}