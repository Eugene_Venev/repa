import java.util.Scanner;

public class TaskCh12N024 {
    static class Outer {
        public static void main(String[] args) {
            Scanner str = new Scanner(System.in);
            System.out.println("Введите размер массива: ");
            int arrSize = str.nextInt();
            another A = new another();
            int[][] x = A.first_24(arrSize);
            int[][] y = A.second_24(arrSize);
            int i, j;
            for (i = 0; i < arrSize; i++) {
                System.out.println(" ");
                for (j = 0; j < arrSize; j++) {
                    System.out.print(x[i][j] + "          ");
                }
            }
            System.out.println(" ");
            for (i = 0; i < arrSize; i++) {
                System.out.println(" ");
                for (j = 0; j < arrSize; j++) {
                    System.out.print(y[i][j] + "          ");
                }
            }
        }
    }


    static class another {

        int[][] first_24(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            for (i = 1; i < arr.length; i++) {
                arr[0][i] = 1;
                arr[i][0] = 1;
            }
            for (i = 1; i < arr.length; i++) {
                for (j = 1; j < arr.length; j++) {
                    arr[i][j] = arr[i - 1][j] + arr[i][j - 1];
                }
            }
            return arr;
        }

        int[][] second_24(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            for (i = 0; i < arr.length; i++) {
                for (j = 0; j < arr.length; j++) {

                    arr[i][j] = (i + j) % arr.length + 1;
                }
            }
            return arr;
        }
    }
}
