import java.util.Scanner;

class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Размер массива: ");
        int[] array = new int[scanner.nextInt()];
        System.out.println("Элементы массива: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Индекс: " + recurs(array, 0));
    }

    private static int recurs(int[] array, int Ind) {
        if (Ind >= array.length - 1) {
            return array.length - 1;
        }

        int maxInd = recurs(array, Ind + 1);
        if (array[maxInd] < array[Ind]) {
            maxInd = Ind;
        }
        return maxInd;
    }
}
