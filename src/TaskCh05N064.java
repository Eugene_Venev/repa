import java.util.Scanner;

public class TaskCh05N064 {


    public static void main(String[] args) {
        int[] area = new int[12];
        int[] population = new int[12];

        Scanner str = new Scanner(System.in);
        for (int i = 0; i < 12; i++) {
            System.out.println("Площaдь : " + (i + 1));
            area[i] = str.nextInt();
            System.out.println("Люди " + (i + 1));
            population[i] = str.nextInt();
        }

        double avgDensity = calculateAvgDensity(area, population);
        System.out.println(avgDensity);
    }

    public static double calculateAvgDensity(int[] area, int[] population) {
        int a = 0;
        int p = 0;
        for (int i = 0; i < area.length; i++) {
            a += area[i];
            p += population[i];
        }
        return 1.0 * p / a;
    }
}