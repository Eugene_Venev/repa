import java.util.Scanner;

public class TaskCh09N185 {


    public static void main(String args[]) {
        Scanner str = new Scanner(System.in);
        System.out.println("Enter the phrase");
        String Str = str.nextLine();                //входная фраза
        char[] STR = new char[Str.length()];        //размер массива определен масимальным количеством искомых элементов
        STR = Str.toCharArray();

        int[] i = new int[Str.length()];            //инициализация рабочих счетчиков
        int[] j = new int[Str.length()];            //
        int l = 0;                                  //
        int m = 0;                                  //
        for (int k = 0; k < STR.length; k++) {      //цикл по всей длине входной фразы
            if (STR[k] == '(') {                    //если выполняется условие нахождения символа
                i[l] = k;                           //то индекс символа в массиве добавляется в счетный массив
                l++;                                //а индекс счетного массива инкрементируется
            } else if (STR[k] == ')') {
                j[m] = k;
                m++;
            }
        }
        if (l == m) {                               //проверяется равенство количества обеих скобок
            for (int o = 0; o < (l); o++) {         //цикл до "количества скобок"
                if (i[o] < j[o]) {
                    if (o == (l - 1)) {                  //условие правильного расположения открывающих/закрывающих скобок
                        System.out.println("Вроде верно, хотя не уверен");
                    }
                }
            }
        } else if (l > m) {                         //дополнительные условия
            System.out.println("Всего " + (l - m) + " лишних левых скобок");
        } else if (l < m) {                         //дополнительные условия
            System.out.println("Первое вхождение правой скобки: " + Str.indexOf(')'));
        }
    }
}
