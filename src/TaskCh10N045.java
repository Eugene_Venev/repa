import java.util.Scanner;

public class TaskCh10N045 {

    static class recurs {
        int arProg(int a1, int d, int an) {
            if (an > 1)
                a1 = d + arProg(a1, d, an - 1);
            else
                return a1;
            return a1;
        }

        int sumNel(int a2, int d2, int an2) {
            if (an2 > 1) {
                a2 = (an2 * d2) + sumNel(a2, d2, an2 - 1);        //апож
            } else
                return a2;
            return a2;
        }
    }

    public static void main(String args[]) {
        recurs f = new recurs();
        Scanner str = new Scanner(System.in);
        System.out.println("Введите число: ");
        int num = str.nextInt();
        System.out.println("Введите шаг прогрессии: ");
        int RAP = str.nextInt();
        System.out.println("Введите номер искомого члена прогрессии: ");
        int count = str.nextInt();
        System.out.println(f.arProg(num, RAP, count));
        System.out.println(f.sumNel(num, RAP, count));
    }
}

