import java.util.Scanner;

public class TaskCh12N025 {
    static class Outer {
        public static void main(String[] args) {
            Scanner str = new Scanner(System.in);
            System.out.println("Введите размер массива: ");
            int arrSize = str.nextInt();
            output o = new output();
            System.out.println("Введите метод извращений Златопольского (цифру[1-16]): ");
            int method = str.nextInt();
            o.outPut(arrSize, method);
        }
    }


    static class another {

        int[][] first_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = 0; i < arr.length; i++) {
                for (j = 0; j < arr.length; j++) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] second_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = 0; j < arr.length; j++) {
                for (i = 0; i < arr.length; i++) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] third_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = 0; i < arr.length; i++) {
                for (j = arr.length - 1; j >= 0; j--) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] fourth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = 0; j < arr.length; j++) {
                for (i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] fifth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = 0; i < arr.length; i++) {
                if (i % 2 == 0) {
                    for (j = 0; j < arr.length; j++) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (j = arr.length - 1; j >= 0; j--) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }

        int[][] sixth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = 0; j < arr.length; j++) {
                if (j % 2 == 0) {
                    for (i = 0; i < arr.length; i++) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (i = arr.length - 1; i >= 0; i--) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }

        int[][] seventh_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = arr.length - 1; i >= 0; i--) {
                for (j = 0; j < arr.length; j++) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] eighth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = arr.length - 1; j >= 0; j--) {
                for (i = 0; i < arr.length; i++) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] ninth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = arr.length - 1; i >= 0; i--) {
                for (j = arr.length - 1; j >= 0; j--) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] tenth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = arr.length - 1; j >= 0; j--) {
                for (i = arr.length - 1; i >= 0; i--) {
                    arr[i][j] = count++;
                }
            }
            return arr;
        }

        int[][] eleventh_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = arrSize - 1; i >= 0; i--) {
                if (i % 2 == 0) {
                    for (j = 0; j < arr.length; j++) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (j = arr.length - 1; j >= 0; j--) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }

        int[][] twelfth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = 0; i < arrSize; i++) {
                if (i % 2 == 0) {
                    for (j = arrSize - 1; j >= 0; j--) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (j = 0; j < arrSize; j++) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }

        int[][] thirteenth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = arrSize - 1; j >= 0; j--) {
                if (j % 2 == 0) {
                    for (i = 0; i < arrSize; i++) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (i = arrSize - 1; i >= 0; i--) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }

        int[][] fourteenth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = 0; j < arrSize; j++) {
                if (j % 2 != 0) {
                    for (i = 0; i < arrSize; i++) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (i = arrSize - 1; i >= 0; i--) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }

        int[][] fifteenth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (i = arrSize - 1; i >= 0; i--) {
                if (i % 2 != 0) {
                    for (j = 0; j < arrSize; j++) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (j = arrSize - 1; j >= 0; j--) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }

        int[][] sixteenth_25(int arrSize) {
            int[][] arr = new int[arrSize][arrSize];
            int i, j;
            int count = 1;
            for (j = arrSize - 1; j >= 0; j--) {
                if (j % 2 != 0) {
                    for (i = 0; i < arrSize; i++) {
                        arr[i][j] = count++;
                    }
                } else {
                    for (i = arrSize - 1; i >= 0; i--) {
                        arr[i][j] = count++;
                    }
                }
            }
            return arr;
        }
    }


    static class output {
        void outPut(int arrSize, int method) {
            another A = new another();
            int[][] first = A.first_25(arrSize);
            int[][] second = A.second_25(arrSize);
            int[][] third = A.third_25(arrSize);
            int[][] fourth = A.fourth_25(arrSize);
            int[][] fifth = A.fifth_25(arrSize);
            int[][] sixth = A.sixth_25(arrSize);
            int[][] seventh = A.seventh_25(arrSize);
            int[][] eighth = A.eighth_25(arrSize);
            int[][] ninth = A.ninth_25(arrSize);
            int[][] tenth = A.tenth_25(arrSize);
            int[][] eleventh = A.eleventh_25(arrSize);
            int[][] twelfth = A.twelfth_25(arrSize);
            int[][] thirteenth = A.thirteenth_25(arrSize);
            int[][] fourteenth = A.fourteenth_25(arrSize);
            int[][] fifteenth = A.fifteenth_25(arrSize);
            int[][] sixteenth = A.sixteenth_25(arrSize);
            int i, j;
            for (i = 0; i < arrSize; i++) {
                System.out.println(" ");
                for (j = 0; j < arrSize; j++) {
                    switch (method) {
                        case 1:
                            System.out.print(first[i][j] + "               ");
                            break;
                        case 2:
                            System.out.print(second[i][j] + "               ");
                            break;
                        case 3:
                            System.out.print(third[i][j] + "               ");
                            break;
                        case 4:
                            System.out.print(fourth[i][j] + "               ");
                            break;
                        case 5:
                            System.out.print(fifth[i][j] + "               ");
                            break;
                        case 6:
                            System.out.print(sixth[i][j] + "               ");
                            break;
                        case 7:
                            System.out.print(seventh[i][j] + "               ");
                            break;
                        case 8:
                            System.out.print(eighth[i][j] + "               ");
                            break;
                        case 9:
                            System.out.print(ninth[i][j] + "               ");
                            break;
                        case 10:
                            System.out.print(tenth[i][j] + "               ");
                            break;
                        case 11:
                            System.out.print(eleventh[i][j] + "               ");
                            break;
                        case 12:
                            System.out.print(twelfth[i][j] + "               ");
                            break;
                        case 13:
                            System.out.print(thirteenth[i][j] + "               ");
                            break;
                        case 14:
                            System.out.print(fourteenth[i][j] + "               ");
                            break;
                        case 15:
                            System.out.print(fifteenth[i][j] + "               ");
                            break;
                        case 16:
                            System.out.print(sixteenth[i][j] + "               ");
                            break;

                    }
                }
            }
        }
    }
}
