import java.util.Scanner;

public class TaskCh10N047 {
    static class recurs {

        int fiboNums(int n) {
            if (n == 0)
                return 0;
            if (n == 1)
                return 1;
            return fiboNums(n - 1) + fiboNums(n - 2);
        }
    }


    static class Recursion {
        public static void main(String args[]) {
            recurs f = new recurs();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите число: ");
            int num = str.nextInt();
            System.out.println(f.fiboNums(num));
        }
    }
}
