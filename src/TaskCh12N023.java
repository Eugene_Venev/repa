import java.util.Scanner;

public class TaskCh12N023 {
    static class Outer {
        public static void main(String[] args) {
            Scanner str = new Scanner(System.in);
            System.out.println("Введите размер массива: ");
            int arrSize = str.nextInt();
            another A = new another();
            int[][] x = A.first_23(arrSize);
            int[][] y = A.second_23(arrSize);
            int[][] z = A.third_23(arrSize);
            int i, j;
            for (i = 0; i < arrSize; i++) {
                System.out.println(" ");
                for (j = 0; j < arrSize; j++) {
                    System.out.print(x[i][j] + " ");
                }
            }
            System.out.println(" ");
            for (i = 0; i < arrSize; i++) {
                System.out.println(" ");
                for (j = 0; j < arrSize; j++) {
                    System.out.print(y[i][j] + " ");
                }
            }
            System.out.println(" ");
            for (i = 0; i < arrSize; i++) {
                System.out.println(" ");
                for (j = 0; j < arrSize; j++) {
                    System.out.print(z[i][j] + " ");

                }
            }
        }

        static class another {

            int[][] first_23(int arrSize) {
                int[][] arr = new int[arrSize][arrSize];
                int i;
                int j;
                for (i = 0; i < arr.length; i++) {
                    for (j = 0; j < arr.length; j++) {
                        if (i == j) {
                            arr[i][j] = 1;
                        } else if (i == (arr.length - 1 - j)) {
                            arr[i][j] = 1;
                        } else if (j == (arr.length - 1 - i)) {
                            arr[i][j] = 1;
                        } else arr[i][j] = 0;
                    }
                }
                return arr;
            }

            int[][] second_23(int arrSize) {
                int[][] arr = new int[arrSize][arrSize];
                int i;
                int j;
                for (i = 0; i < arr.length; i++) {
                    for (j = 0; j < arr.length; j++) {
                        if (i == j) {
                            arr[i][j] = 1;
                        } else if (i == (arr.length - 1 - j)) {
                            arr[i][j] = 1;
                        } else if (j == (arr.length - 1 - i)) {
                            arr[i][j] = 1;
                        } else if (i == arr.length / 2) {
                            arr[i][j] = 1;
                        } else if (j == arr.length / 2) {
                            arr[i][j] = 1;
                        } else arr[i][j] = 0;
                    }
                }
                return arr;
            }

            int[][] third_23(int arrSize) {
                int[][] arr = new int[arrSize][arrSize];
                int i;
                int j;
                for (i = 0; i < arr.length; i++) {
                    for (j = i; j < arr.length - i; j++) {
                        arr[i][j] = 1;
                    }
                    for (j = arr.length - 1 - i; j <= i; j++) {
                        arr[i][j] = 1;
                    }
                }
                return arr;
            }
        }
    }
}
