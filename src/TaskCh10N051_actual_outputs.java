public class TaskCh10N051_actual_outputs {
    static class recurs {
        int n = 5;

        public static void proc1(int n) {
            if (n > 0) {
                System.out.println(n);
                proc1(n - 1);
            }
        }

        public static void proc2(int n) {
            if (n > 0) {
                proc2(n - 1);
                System.out.println(n);
            }
        }

        public static void proc3(int n) {
            if (n > 0) {
                System.out.println(n);
                proc3(n - 1);
            }
            System.out.println(n);
        }
    }

    public static void main(String[] args) {
        recurs f = new recurs();
        recurs.proc1(5);
        f.proc2(5);
        f.proc3(5);
    }
}

