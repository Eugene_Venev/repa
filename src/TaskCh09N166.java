import java.util.Scanner;

public class TaskCh09N166 {


    public static void main(String args[]) {
        Scanner str = new Scanner(System.in);
        System.out.println("Enter the string");
        String Str = str.nextLine();
        System.out.println(stringSplit(Str));
    }

    static String stringSplit(String Str) {
        String[] result = Str.split(" ");
        String swapWords = result[0];
        result[0] = result[result.length - 1];
        result[result.length - 1] = swapWords;
        return String.join(" ", result);
    }
}