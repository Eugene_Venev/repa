import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        new Game().play();
    }
}

class Team {
    public int score;
    public String name;

    Team(String name) {
        int score;
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

}


class Game {

    Team team1;
    Team team2;

    void play() {
        Scanner str = new Scanner(System.in);

        System.out.println("Enter team 1: ");
        team1 = new Team(str.nextLine());

        System.out.println("Enter team 2: ");
        team2 = new Team(str.nextLine());
        while (true) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");

            String teamNumber = str.nextLine();

            if ("0".equals(teamNumber)) {
                break;
            } else if (teamNumber.equals("1")) {
                System.out.println("Enter score: ");
                team1.score += str.nextInt();
                System.out.println(Score());
            } else if (teamNumber.equals("2")) {
                System.out.println("Enter score: ");
                team2.score += str.nextInt();
                System.out.println(Score());
            }
        }
        System.out.println(result());


    }

    String Score() {
        return (team1.getScore() + " - " + team2.getScore());
    }


    String result() {
        if (team1.getScore() == team2.getScore())
            return ("ничья : " + Score());
        else if (team1.getScore() > team2.getScore())
            return ("победитель " + team1.getName() +" "+ team1.getScore());
        else return ("победитель " + team2.getName() +" "+ team2.getScore());
    }
}

