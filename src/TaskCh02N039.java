import java.util.Scanner;

class TaskCh02N039 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);

        System.out.println("часы ");
        int hours = str.nextInt();

        System.out.println("минуты");
        float mins = str.nextInt();

        System.out.println("секунды");
        float secs = str.nextInt();

        float degrees = (30 * hours) + (30 * (mins / 60)) + (30 * (secs / 3600));

        System.out.println("градусов " + degrees);


    }
}
