import java.util.Scanner;

public class TaskCh09N042 {

    public static void main(String args[]) {
        Scanner str = new Scanner(System.in);
        System.out.println("Enter the string");
        String Str = str.nextLine();
        StringBuilder STR = new StringBuilder(Str);
        STR.reverse();
        System.out.println(STR);
    }
}
