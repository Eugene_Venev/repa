import java.util.Scanner;

public class TaskCh10N046 {

    static class recurs {
        int geoProg(int b, int q, int n) {
            int result;
            if (n > 1)
                result = q * geoProg(b, q, n - 1);
            else
                return b;
            return result;
        }

        double getSumProg(int b, int q, int n) {
            if (n == 0) return 0;
            return getSumProg(b, q, n - 1) + b * Math.pow(q, n - 1);
        }
    }

    public static void main(String args[]) {
        recurs f = new recurs();
        Scanner str = new Scanner(System.in);
        System.out.println("Первый член прогрессии: ");
        int num = str.nextInt();
        System.out.println("Знаменатель: ");
        int div = str.nextInt();
        System.out.println("Введите номер искомого члена прогрессии: ");
        int count = str.nextInt();
        System.out.println(f.geoProg(num, div, count));
        System.out.println(f.getSumProg(num, div, count));
    }
}