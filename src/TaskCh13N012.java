import java.time.Period;
import java.util.ArrayList;
import java.time.LocalDate;
//import java.util.Scanner;

public class TaskCh13N012 {
    public static void main(String[] args) {

        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Путин", "Владимир", "Владимирович", LocalDate.of(2000, 12, 1)));
        employees.add(new Employee("Медведев", "Дмитрий", "Анатольевич", LocalDate.of(2008, 5, 1)));
        employees.add(new Employee("Хренова", "Гадя", "Петрович", LocalDate.of(2017, 3, 1)));
        employees.add(new Employee("Кшыштопоповжецкая", "Изольда", LocalDate.of(2017, 6, 1)));

        //for (Employee employee : employees){
        //    (employee.getWorkingExperienceOnDate()>3).forEach(System.out::println);
        //}
        employees.stream()
                .filter(employee -> employee.getWorkingExperienceOnDate() >= 3)
                .forEach(System.out::println);
    }
}


class Employee {
    String surname;
    String name;
    String patronymic;
    private LocalDate jobDate;

    public Employee(String surname, String name, LocalDate jobDate) {
        this.surname = surname;
        this.name = name;
        this.patronymic = "";
        this.jobDate = jobDate;
    }

    public Employee(String surname, String name, String patronymic, LocalDate jobDate) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.jobDate = jobDate;
    }

    /*public String getName() {
        return name;
    }

    public String getFullName() {
        return surname + name + patronymic;
    }*/

    public int getWorkingExperienceOnDate() {
        return Period.between(jobDate, LocalDate.now()).getYears();
    }

    @Override
    public String toString() {
        return name + " " + patronymic + " " + surname + ", стаж  : " + getWorkingExperienceOnDate();
    }
}


