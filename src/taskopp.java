import java.util.ArrayList;
import java.util.List;

public class taskopp {

    public static void main(String[] args) {
        Employer employer = new Employer();
        List<Candidate> candidates = new ArrayList<>();
        candidates.add(new Candidate("Vasya", "self-learner"));     //список из 10 человек
        candidates.add(new Candidate("Petya", "getJavaCourses"));
        candidates.add(new Candidate("Slim Shady", "self-learner"));
        candidates.forEach(candidate -> {
            employer.hello();
            candidate.hello();
            candidate.describeExperience();
        });
    }
}

class Candidate {

    String name;
    String exp;

    public Candidate(String name, String exp) {
        this.name = name;
        this.exp = exp;
    }

    public void hello() {
        System.out.println("Hi, my name is " + name);
    }

    public void describeExperience() {
        if (exp.equals("self-learner")) {
            System.out.println("I have been learning java by myself, nobody examined how thorough is my knowledge and how good is my code.");
            System.out.println(" ");
        } else {
            System.out.println("I passed successfully getJavaJob exams and code reviews ");
            System.out.println(" ");
        }
    }
}

class Employer {
    public void hello() {
        System.out.println("Hi! Introduce yourself");
    }
}