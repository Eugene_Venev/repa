import java.util.Scanner;

public class TaskCh10N052 {
    static class recurs {

        int inverse(int n) {
            if (n == 0)
                return 0;
            System.out.print(n % 10);
            return inverse(n / 10);
        }
    }

    static class Recursion {
        public static void main(String args[]) {
            recurs r = new recurs();
            Scanner str = new Scanner(System.in);
            System.out.println("Введите число: ");
            int num = str.nextInt();
            System.out.println(r.inverse(num));
        }
    }
}
