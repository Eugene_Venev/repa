import java.util.Scanner;

public class TaskCh09N107 {


    public static void main(String args[]) {
        Scanner str = new Scanner(System.in);
        System.out.println("Enter the string");
        String Str = str.nextLine();
        StringBuilder STR = new StringBuilder(Str);
        int a1 = Str.indexOf('a');
        int o1 = Str.lastIndexOf('o');
        if ((a1 == -1) | (o1 == -1)) {
            System.out.println("Нет искомой буквы");
        } else {
            STR.setCharAt(a1, 'o');
            STR.setCharAt(o1, 'a');
            System.out.println(STR);
        }
    }
}

