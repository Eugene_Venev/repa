import java.util.Scanner;

class TaskCh04N115_optimized {

    public static void main(String[] args) {

        Scanner str = new Scanner(System.in);
        System.out.println("Год: ");
        int year = str.nextInt();
        int yearEast = (year % 60) - 4;
        int element = (year % 60) / 12;
        if (yearEast < 0) yearEast += 12;
        else if (yearEast > 11) yearEast = yearEast % 12;

        System.out.println(getYearName(yearEast) + ", " + getColor(element));

    }

    private static String getColor(int element) {
        switch (element) {
            case 0: return "tree";
            case 1: return "fire";
            case 2: return "earth";
            case 3: return "metall";
            case 4: return "water ";
        }
        throw new UnsupportedOperationException("Unsupported operation for 'element' " + element);
    }

    private static String getYearName(int yearEast) {
        switch (yearEast) {
            case 0: return "Rat";
            case 1: return "Bull";
            case 2: return "Tiger";
            case 3: return "Rabbit";
            case 4: return "Dragon";
            case 5: return "Snake";
            case 6: return "Horse";
            case 7: return "Sheep";
            case 8: return "Monkey";
            case 9: return "Cock";
            case 10: return "Dog";
            case 11: return "Pig";
        }
        throw new UnsupportedOperationException("Unsupported operation for 'yearEast' " + yearEast);
    }

}