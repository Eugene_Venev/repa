import java.util.Scanner;

class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.println("Строка: " );
        String obj1 = str.nextLine();
        System.out.println("Номер: " );
        int Kel = str.nextInt();
        System.out.println("k-ый элемент строки: " + obj1.charAt(Kel - 1));
    }
}