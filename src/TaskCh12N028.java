import java.util.Scanner;

public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int arrSize = str.nextInt();
        int[][] arr = new int[arrSize][arrSize];
        int count = arrSize * arrSize;
        int i = arrSize / 2;
        int j = arrSize / 2;
        int indUp = i;
        int indDown = i;
        int indLeft = j;
        int indRight = j;
        int direction = 1;
        for (int a = count; a > 0; a--) {
            arr[i][j] = a;
            switch (direction) {
                case 0:
                    i -= 1;
                    if (i < indUp) {
                        direction = 1;
                        indUp = i;
                    }
                    break;
                case 1:
                    j -= 1;
                    if (j < indLeft) {
                        direction = 2;
                        indLeft = j;
                    }
                    break;
                case 2:
                    i += 1;
                    if (i > indDown) {
                        direction = 3;
                        indDown = i;
                    }
                    break;
                case 3:
                    j += 1;
                    if (j > indRight) {
                        direction = 0;
                        indRight = j;
                    }
            }
        }
        for (int k = 0; k < arrSize; k++) {
            for (int l = 0; l < arrSize; l++)
                System.out.print(arr[k][l] + "         ");
            System.out.println();
        }
    }
}